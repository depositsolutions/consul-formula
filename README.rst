======
consul
======
.. image:: https://gitlab.com/depositsolutions/consul-formula/badges/master/build.svg
    :target: https://gitlab.com/depositsolutions/consul-formula/pipelines

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

    This formula is supporting saltstack version 2017.7, delete the _modules folder to use it with 2018..

Available states
================

.. contents::
    :local:

``consul``
------------

Installs and configures the Consul service.

``consul.install``
------------------

Downloads and installs the Consul binary file.

``consul.config``
-----------------

Provision the Consul configuration files and sources.

``consul.service``
------------------

Adds the Consul service startup configuration or script to an operating system.

To start a service during Salt run and enable it at boot time, you need to set following Pillar:

.. code:: yaml

    consul:
      service: True

``consul.acl``
-----------------

Create ACL tokens with rules.

.. code:: yaml

    consul:
      acl_tokens:
        test1-token:
          token_name: "Test 1"
          token_type: client
          token_id: 707CBB61-C665-4EE4-9210-819BFEBF670A
          token_rules: '"node \"\" { policy = \"write\" } service \"\" { policy = \"read\" }"'

``consul.kvstore``
-----------------

Put Key/Values into consul.

.. code:: yaml

    consul:
      kvstore:
        foo:
          bar:
            rocks: 'forever'

Contributor
================

``kvstore python script``
-----------------
https://gitlab.com/MartinOlschewski
