consul:
  # Start Consul agent service and enable it at boot time
  service: True

  version: 1.3.1

  config:
    server: True
    bind_addr: 0.0.0.0
    client_addr: 0.0.0.0
    enable_debug: True
    log_level: INFO
    datacenter: eu
    encrypt: "RIxqpNlOXqtr/j4BgvIMEw=="
    bootstrap_expect: 1
    retry_interval: 15s
    node_name: test-consul
    addresses:
      # Allowing local connections for admin procedures
      http: '0.0.0.0'
    # retry_join:
    #   - 1.1.1.1
    #   - 2.2.2.2

  # The register key can be used to manually register apps
  register:
    - name: myapp
      checks:
        - http: http://localhost:8500/v1/agent/members
          interval: 10s
        #    - name: Redis
        #      checks:
        #        - script: /usr/local/share/consul/check_redis.py
        #          interval: 10s
        #  scripts:
        #    - source: salt://files/consul/check_redis.py
        #      name: /usr/local/share/consul/check_redis.py

# Fill keyvalue store with data
#  kvstore:
#    foo:
#      bar:
#        rocks: 'forever'
