describe user('consul') do
  it { should exist }
end

describe group('consul') do
  it { should exist }
  its('gid') { should be < 1000 }

end

describe file('/etc/consul.d') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/consul.d/config.json') do
 it { should exist }
 it { should be_owned_by 'consul' }
end

describe file('/etc/consul.d/services.json') do
 it { should exist }
 it { should be_owned_by 'consul' }
end

describe file('/usr/local/bin/consul') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe service('consul') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
